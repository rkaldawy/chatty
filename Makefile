INC_DIR = ./include

LIBS= -lpthread -lcrypto
CFLAGS= -Wall -I$(INC_DIR)

SRC=$(wildcard src/*.cpp)

all: chatty

chatty: $(SRC)
	g++ -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -rf chatty

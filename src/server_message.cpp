#include "server_message.hpp"
#include "utilities.hpp"

using namespace utilities;

/****************************************************************************************/

ServerMessage::ServerMessage(servermsg_t _type, uint32_t id) : Message(id, SERVER)
{
  s_type = _type;
}
/****************************************************************************************/

ServerMessage::ServerMessage(servermsg_t _type, uint32_t id, struct timeval tv)
    : Message(id, SERVER, tv)
{
  s_type = _type;
}
/****************************************************************************************/

vector<vector<uint8_t>> ServerMessage::pack_to_bytes()
{
  vector<uint8_t> out = pack_to_bytes_hlpr();
  return {out};
}
/****************************************************************************************/

vector<uint8_t> ServerMessage::pack_to_bytes_hlpr()
{
  vector<uint8_t> out = Message::pack_to_bytes_hlpr();
  out.push_back(static_cast<uint8_t>(s_type));

  return out;
}
/****************************************************************************************/

ServerMessage *ServerMessage::load_from_bytes(uint8_t *buf)
{
  uint32_t _sender = *(reinterpret_cast<uint32_t *>(&buf[SENDER_OFFSET - 1]));
  struct timeval _send_time =
      *(reinterpret_cast<struct timeval *>(&buf[TIME_OFFSET - 1]));

  servermsg_t _s_type = static_cast<servermsg_t>(buf[STYPE_OFFSET - 1]);

  switch (_s_type) {
  case REGISTER_NAME: {
    //  uint32_t _id = *(reinterpret_cast<uint32_t *>(&buf[RegisterName_SM::ID_OFFSET -
    //  1]));
    string _nname((char *)&buf[RegisterName_SM::NICKNAME_OFFSET - 1]);

    return new RegisterName_SM(_sender, _send_time, _nname);
  }
  case RESPONSE: {
    bool _resp = static_cast<bool>(&buf[Response_SM::RESP_OFFSET - 1]);

    return new Response_SM(_sender, _send_time, _resp);
  }
  case CONNECTION: {
    uint32_t _client =
        *(reinterpret_cast<uint32_t *>(&buf[Connection_SM::ID_OFFSET - 1]));
    bool _status = static_cast<bool>(buf[Connection_SM::STATUS_OFFSET - 1]);

    return new Connection_SM(_sender, _send_time, _client, _status);
  }

  default:
    return NULL;
  }
}
/****************************************************************************************/

RegisterName_SM::RegisterName_SM(uint32_t _id, string _nname)
    : ServerMessage(REGISTER_NAME, _id)
{
  id = _id;
  nname = _nname;
}
/****************************************************************************************/

RegisterName_SM::RegisterName_SM(uint32_t _id, struct timeval tv, string _nname)
    : ServerMessage(REGISTER_NAME, _id, tv)
{
  id = _id;
  nname = _nname;
}
/****************************************************************************************/

vector<vector<uint8_t>> RegisterName_SM::pack_to_bytes()
{
  vector<uint8_t> out = ServerMessage::pack_to_bytes_hlpr();

  uint8_t buf[DATA_SIZE] = {0};
  append_integer_to_bytes(id, buf);
  strcpy((char *)buf + 4, nname.c_str());

  out.insert(out.end(), &buf[0], &buf[0] + DATA_SIZE);

  return {out};
}
/****************************************************************************************/

Response_SM::Response_SM(uint32_t _id, bool _resp) : ServerMessage(RESPONSE, _id)
{
  resp = _resp;
}
/****************************************************************************************/

Response_SM::Response_SM(uint32_t _id, struct timeval tv, bool _resp)
    : ServerMessage(RESPONSE, _id, tv)
{
  resp = _resp;
}
/****************************************************************************************/

vector<vector<uint8_t>> Response_SM::pack_to_bytes()
{
  vector<uint8_t> out = ServerMessage::pack_to_bytes_hlpr();

  uint8_t buf[DATA_SIZE] = {0};
  buf[0] = static_cast<uint8_t>(resp);
  out.insert(out.end(), &buf[0], &buf[0] + DATA_SIZE);

  return {out};
}
/****************************************************************************************/

Connection_SM::Connection_SM(uint32_t _id, uint32_t _client, bool _status)
    : ServerMessage(CONNECTION, _id)
{
  client = _client;
  status = _status;
}
/****************************************************************************************/

Connection_SM::Connection_SM(uint32_t _id, struct timeval _tv, uint32_t _client,
                             bool _status)
    : ServerMessage(CONNECTION, _id, _tv)
{
  client = _client;
  status = _status;
}
/****************************************************************************************/

vector<vector<uint8_t>> Connection_SM::pack_to_bytes()
{
  vector<uint8_t> out = ServerMessage::pack_to_bytes_hlpr();

  uint8_t buf[DATA_SIZE] = {0};
  append_integer_to_bytes(client, &buf[0]);
  buf[4] = static_cast<uint8_t>(status);
  out.insert(out.end(), &buf[0], &buf[0] + DATA_SIZE);

  return {out};
}
/****************************************************************************************/

#include "client.hpp"
#include "server.hpp"

int main(int argc, char **argv)
{
  srand(time(NULL));

  if (argc < 2) {
    cout << "Usage: ./chatty server port" << endl;
    cout << "Usage: ./chatty client ip port" << endl;
    return -1;
  }

  if (!strcmp("server", argv[1])) {
    Server server;
    if (argc >= 3)
      server.run_server(strtol(argv[2], NULL, 10));
  } else if (!strcmp("client", argv[1])) {
    Client client;
    if (argc >= 4)
      client.run_client(argv[2], strtol(argv[3], NULL, 10));
  }
}

#include "state_functors.hpp"
#include "client.hpp"
#include "server.hpp"

template class FunctorStateMap<decltype(&ClientStateContext::DEFAULT_RECV_Functor)>;
template class FunctorStateMap<decltype(&ClientStateContext::DEFAULT_SEND_Functor)>;
template class FunctorStateMap<decltype(&Server::DEFAULT_Functor)>;

/****************************************************************************************/

template <typename T>
FunctorTypeMap<T> &FunctorStateMap<T>::operator[](state_t key)
{
  typename std::map<state_t, FunctorTypeMap<T>>::iterator it;
  it = f_map.find(key);
  if (it == f_map.end()) {}
  return f_map[key];
}
/****************************************************************************************/

template <typename T>
FunctorWrapper<T> &FunctorTypeMap<T>::operator[](servermsg_t key)
{
  typename std::map<servermsg_t, FunctorWrapper<T>>::iterator it;
  it = f_map.find(key);
  if (it == f_map.end()) {}
  return f_map[key];
}
/****************************************************************************************/

template <typename T>
T FunctorStateMap<T>::fetch_func(state_t state)
{
  T ret;
  bool result = _fetch_func(state, ALL, ret);
  if (!result) {
    return def;
  }
  return ret;
}
/****************************************************************************************/

template <typename T>
T FunctorStateMap<T>::fetch_func(state_t state, servermsg_t type)
{
  T ret;
  bool result = _fetch_func(state, type, ret);
  if (!result) {
    return def;
  }
  return ret;
}
/****************************************************************************************/

template <typename T>
bool FunctorStateMap<T>::_fetch_func(state_t state, servermsg_t type, T &ret)
{
  typename std::map<state_t, FunctorTypeMap<T>>::iterator it;
  it = f_map.find(state);
  if (it == f_map.end()) {
    return false;
  }

  return (*this)[state]._fetch_func(type, ret);
}
/****************************************************************************************/

template <typename T>
bool FunctorTypeMap<T>::_fetch_func(servermsg_t type, T &ret)
{
  typename std::map<servermsg_t, FunctorWrapper<T>>::iterator it;
  it = f_map.find(ALL);
  if (it != f_map.end()) {
    return (*this)[ALL]._fetch_func(ret);
  }

  it = f_map.find(type);
  if (it == f_map.end()) {
    return false;
  }

  return (*this)[type]._fetch_func(ret);
}
/****************************************************************************************/

template <typename T>
bool FunctorWrapper<T>::_fetch_func(T &ret)
{
  ret = func;
  return true;
}

/****************************************************************************************/

DEFINE_CLIENT_SENDER_FUNCTOR(START, ALL)
{
  string s;
  cout << "Enter a nickname: ";
  getline(cin, s);

  client->nickname_map[client->id] = s;
  ServerMessage *send_msg = new RegisterName_SM(client->id, s);
  state = REGISTERING_NAME;

  return send_msg;
}
/****************************************************************************************/

DEFINE_CLIENT_SENDER_FUNCTOR(REGISTERING_NAME, ALL)
{
  wait_for_reciever();
  return NULL;
}
/****************************************************************************************/

DEFINE_CLIENT_RECEIVER_FUNCTOR(REGISTERING_NAME, RESPONSE)
{
  cout << "Hello" << endl;
  Response_SM *msg = reinterpret_cast<Response_SM *>(recv_msg);

  if (msg->resp) {
    state = RECEIVING_CONTEXT;
  } else {
    state = START;
    wakeup_sender();
  }
}
/****************************************************************************************/

DEFINE_CLIENT_RECEIVER_FUNCTOR(RECEIVING_CONTEXT, REGISTER_NAME)
{
  RegisterName_SM *msg = reinterpret_cast<RegisterName_SM *>(recv_msg);
  client->nickname_map[msg->id] = msg->nname;
}
/****************************************************************************************/

DEFINE_CLIENT_RECEIVER_FUNCTOR(RECEIVING_CONTEXT, RESPONSE)
{
  Response_SM *msg = reinterpret_cast<Response_SM *>(recv_msg);
  if (msg->resp) {
    initialized = true;
    state = READY;
    wakeup_sender();
  }
}
/****************************************************************************************/

DEFINE_CLIENT_RECEIVER_FUNCTOR(READY, REGISTER_NAME)
{
  RegisterName_SM *msg = reinterpret_cast<RegisterName_SM *>(recv_msg);
  client->nickname_map[msg->id] = msg->nname;
}
/****************************************************************************************/

DEFINE_CLIENT_RECEIVER_FUNCTOR(READY, CONNECTION)
{
  Connection_SM *msg = reinterpret_cast<Connection_SM *>(recv_msg);

  if (msg->status) {
    printf("\n%s has connected.\n", client->nickname_map[msg->client].c_str());
  } else {
    printf("\n%s has disconnected.\n", client->nickname_map[msg->client].c_str());
  }
}
/****************************************************************************************/

DEFINE_SERVER_FUNCTOR(START, REGISTER_NAME)
{
  RegisterName_SM *msg = reinterpret_cast<RegisterName_SM *>(recv_msg);

  origin->client_id = msg->id;
  nickname_map[msg->id] = msg->nname;
  //@todo: write some helpers that do different types of sends

  ServerMessage *send_msg;
  send_msg = new Response_SM(0, true);
  send_msg->populate_timestamp();
  origin->out_queue.push(send_msg, true);

  send_msg = new RegisterName_SM(msg->id, msg->nname);
  send_msg->populate_timestamp();
  for (int j = 0; j < get_conns_size(); j++) {
    if (conns[j] != origin) {
      conns[j]->out_queue.push(send_msg, true);
    }
  }

  map<uint32_t, string>::iterator it = nickname_map.begin();
  while (it != nickname_map.end()) {
    send_msg = new RegisterName_SM(it->first, it->second);
    send_msg->populate_timestamp();
    origin->out_queue.push(send_msg, true);
    it++;
  }

  send_msg = new Response_SM(0, true);
  send_msg->populate_timestamp();
  origin->out_queue.push(send_msg, true);

  send_msg = new Connection_SM(0, msg->id, true);
  send_msg->populate_timestamp();
  for (int j = 0; j < get_conns_size(); j++) {
    if (conns[j] != origin) {
      conns[j]->out_queue.push(send_msg, true);
    }
  }

  origin->state = READY;
}

#include "utilities.hpp"
#include "message.hpp"

using namespace utilities;

namespace utilities
{

vector<uint8_t> generate_md5(vector<uint8_t> buf)
{
  char *data = reinterpret_cast<char *>(buf.data());
  int dataLen = buf.size();

  EVP_MD_CTX *ctx = NULL;
  const EVP_MD *mdType = EVP_md5();

  uint8_t md[EVP_MD_size(mdType)];

  // md = (unsigned char *)malloc(*mdLen);
  ctx = EVP_MD_CTX_create();

  EVP_MD_CTX_init(ctx);
  EVP_DigestInit_ex(ctx, mdType, NULL);
  EVP_DigestUpdate(ctx, data, dataLen);
  EVP_DigestFinal_ex(ctx, md, NULL);
  EVP_MD_CTX_destroy(ctx);

  vector<uint8_t> hash(md, md + (sizeof(uint8_t) * EVP_MD_size(mdType)));

  return hash;
}
/****************************************************************************************/

void append_integer_to_bytes(uint32_t val, vector<uint8_t> &result)
{
  result.push_back((val >> 0) & 0xFF);
  result.push_back((val >> 8) & 0xFF);
  result.push_back((val >> 16) & 0xFF);
  result.push_back((val >> 24) & 0xFF);
}
/****************************************************************************************/

void append_integer_to_bytes(uint32_t val, uint8_t *result)
{
  result[0] = ((val >> 0) & 0xFF);
  result[1] = ((val >> 8) & 0xFF);
  result[2] = ((val >> 16) & 0xFF);
  result[3] = ((val >> 24) & 0xFF);
}
/****************************************************************************************/

void append_time_to_bytes(const struct timeval &tv, vector<uint8_t> &result)
{
  uint8_t *ptr = (unsigned char *)&tv;
  result.insert(result.end(), ptr, ptr + sizeof(struct timeval));
}
/****************************************************************************************/

template <typename T>
SemaphoreQueue<T>::SemaphoreQueue() : queue<T>()
{
  pthread_mutex_init(&queue_lock, NULL);
  sem_init(&empty_sem, 0, 0);
  sem_init(&full_sem, 0, MAX_SIZE - 1);
}
/****************************************************************************************/

template <typename T>
SemaphoreQueue<T>::~SemaphoreQueue()
{
  sem_destroy(&empty_sem);
  sem_destroy(&full_sem);
}
/****************************************************************************************/

template <typename T>
int SemaphoreQueue<T>::push(const T &val, bool trywait)
{
  int ret = 0;
  pthread_mutex_lock(&queue_lock);
  if (trywait) {
    ret = sem_trywait(&full_sem);
  } else {
    pthread_mutex_unlock(&queue_lock);
    sem_wait(&full_sem);
    pthread_mutex_lock(&queue_lock);
  }

  queue<T>::push(val);
  sem_post(&empty_sem);

  pthread_mutex_unlock(&queue_lock);

  return ret;
}
/****************************************************************************************/

template <typename T>
int SemaphoreQueue<T>::pop(T *val, bool trywait)
{
  int ret = 0;
  pthread_mutex_lock(&queue_lock);
  if (trywait) {
    ret = sem_trywait(&empty_sem);
  } else {
    pthread_mutex_unlock(&queue_lock);
    sem_wait(&empty_sem);
    pthread_mutex_lock(&queue_lock);
  }

  if (!ret) {
    *val = queue<T>::front()->clone();
    queue<T>::pop();
  }
  sem_post(&full_sem);
  pthread_mutex_unlock(&queue_lock);

  return ret;
}
/****************************************************************************************/

template class SemaphoreQueue<Message *>;
} // namespace utilities

#include "server.hpp"
#include "message.hpp"
#include "server_message.hpp"

Server::Server()
{
  pthread_mutex_init(&conn_lock, NULL);
  sem_init(&conn_sem, 0, 0);

  functors.set_default(&Server::DEFAULT_Functor);
  register_functors();
}
/****************************************************************************************/

void Server::register_functors()
{
  /* Register state behavior for the sender's state handler. */
  REGISTER_SERVER_FUNCTOR(functors, START, REGISTER_NAME);
}
/****************************************************************************************/

void Server::run_server(int port)
{
  // todo:add the fair round robin message handler
  pthread_create(&handler_thread, NULL, connection_handler_thread, this);

  int server_fd, conn_fd;
  struct sockaddr_in address;

  int opt = 1;
  int addrlen = sizeof(address);

  server_fd = socket(AF_INET, SOCK_STREAM, 0);

  setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(port);

  bind(server_fd, (struct sockaddr *)&address, sizeof(address));
  listen(server_fd, 3);

  while (true) {
    conn_fd = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen);

    ServerConnection *conn = new ServerConnection(conn_fd);
    conn->start_connection();

    pthread_mutex_lock(&conn_lock);
    conns.push_back(conn);
    sem_post(&conn_sem);
    pthread_mutex_unlock(&conn_lock);
  }
}
/****************************************************************************************/

int Server::get_conns_size()
{
  int ret;
  pthread_mutex_lock(&conn_lock);
  ret = conns.size();
  pthread_mutex_unlock(&conn_lock);

  return ret;
}
/****************************************************************************************/

void *Server::connection_handler_thread(void *args)
{
  Server *server = reinterpret_cast<Server *>(args);
  int conn_idx = 0;

  while (true) {

    sem_wait(&server->conn_sem);

    pthread_mutex_lock(&server->conn_lock);
    ServerConnection *conn = server->conns[conn_idx];
    pthread_mutex_unlock(&server->conn_lock);

    if (conn->socket_closed) {
      pthread_mutex_lock(&server->conn_lock);
      vector<ServerConnection *>::iterator it = server->conns.begin() + conn_idx;
      uint32_t client = (*it)->client_id;
      delete *it;
      server->conns.erase(it);
      pthread_mutex_unlock(&server->conn_lock);

      for (int j = 0; j < server->get_conns_size(); j++) {
        Connection_SM *msg = new Connection_SM(0, client, false);
        server->conns[j]->out_queue.push(msg, true);
      }
      continue;
    }

    for (int i = 0; i < MAX_MESSAGES_HANDLED; i++) {
      Message *msg;
      if (conn->in_queue.pop(&msg, true)) {
        break;
      }
      server->handle_message(conn, msg);
    }
    sem_post(&server->conn_sem);

    if (conn_idx >= server->get_conns_size() - 1) {
      conn_idx = 0;
    } else {
      conn_idx++;
    }
  }

  return NULL;
}
/****************************************************************************************/

void Server::handle_message(ServerConnection *origin, Message *recv_msg)
{
  switch (recv_msg->get_type()) {
  case Message::SERVER:
    handle_server_message(origin, recv_msg);
    break;
  case Message::BUFFER:
  case Message::CLIENT:
    for (int j = 0; j < get_conns_size(); j++) {
      //@todo: handle case where queue is full
      if (conns[j] != origin) {
        conns[j]->out_queue.push(recv_msg, true);
      }
    }
    break;
  default:;
  }
}
/****************************************************************************************/

void Server::handle_server_message(ServerConnection *origin, Message *recv_msg)
{
  ServerMessage *_msg = reinterpret_cast<ServerMessage *>(recv_msg);
  (this->*functors.fetch_func(origin->state, _msg->s_type))(origin, _msg);
}
/****************************************************************************************/

ServerConnection::ServerConnection(int _conn_fd)
{
  socket_closed = false;
  conn_fd = _conn_fd;
  state = START;
  initialized = false;
}
/****************************************************************************************/

Message *ServerConnection::read_message(int sockfd)
{
  int result = 0;

  uint8_t _type;
  result = read(sockfd, &_type, sizeof(uint8_t));
  if (result <= 0) {
    throw SocketClosedException();
  }
  Message::MESSAGE_TYPE type = static_cast<Message::MESSAGE_TYPE>(_type);

  switch (type) {
  case Message::SERVER: {
    uint8_t buf[ServerMessage::MESSAGE_SIZE - 1];
    memset(buf, 0, ServerMessage::MESSAGE_SIZE - 1);

    result = read(sockfd, buf, ServerMessage::MESSAGE_SIZE - 1);
    if (result <= 0) {
      throw SocketClosedException();
    }

    return ServerMessage::load_from_bytes(buf);
  }
  case Message::CLIENT: {
    uint8_t bytes[ClientMessage::MESSAGE_SIZE - 1];
    memset(bytes, 0, ClientMessage::MESSAGE_SIZE - 1);

    result = read(sockfd, bytes, ClientMessage::MESSAGE_SIZE - 1);
    if (result <= 0) {
      throw SocketClosedException();
    }

    BufferMessage *ret = new BufferMessage(type, bytes, ClientMessage::MESSAGE_SIZE);
    return ret;
  }
  case Message::CONFIRM: {
    uint8_t bytes[ConfirmMessage::MESSAGE_SIZE - 1];
    memset(bytes, 0, ClientMessage::MESSAGE_SIZE - 1);

    result = read(sockfd, bytes, ConfirmMessage::MESSAGE_SIZE - 1);
    if (result <= 0) {
      throw SocketClosedException();
    }

    BufferMessage *ret = new BufferMessage(type, bytes, ConfirmMessage::MESSAGE_SIZE);
    return ret;
  }
  default:
    return NULL;
  }

  return NULL;
}
/****************************************************************************************/

void ServerConnection::start_connection()
{
  pthread_create(&in_thread, NULL, in_handler_thread, this);
  pthread_create(&out_thread, NULL, out_handler_thread, this);
}
/****************************************************************************************/

void *ServerConnection::in_handler_thread(void *args)
{
  ServerConnection *conn = reinterpret_cast<ServerConnection *>(args);
  while (true) {
    try {
      Message *recv_msg = conn->read_message(conn->conn_fd);
      conn->in_queue.push(recv_msg);
    } catch (SocketClosedException &e) {
      conn->socket_closed = true;
    }
  }
}
/****************************************************************************************/

void *ServerConnection::out_handler_thread(void *args)
{
  ServerConnection *conn = reinterpret_cast<ServerConnection *>(args);
  while (true) {
    Message *send_msg = NULL;
    conn->out_queue.pop(&send_msg);

    for (auto &msg : send_msg->pack_to_bytes()) {
      send(conn->conn_fd, &msg[0], send_msg->get_message_size(), 0);
    }

    delete send_msg;
  }
}
/****************************************************************************************/

#include "client.hpp"
#include "state_functors.hpp"

using namespace utilities;

Client::Client() : state_ctx(this)
{
  // state_ctx = ClientStateContext(this);
  counter = 0;
  id = rand();
  socket_closed = false;
  pthread_mutex_init(&msg_lock, NULL);
  pthread_mutex_init(&send_lock, NULL);
}
/****************************************************************************************/

int Client::insert_message_in_chat(Message *message)
{
  std::vector<Message *>::iterator it =
      std::lower_bound(msg_list.begin(), msg_list.end(), message);
  msg_list.insert(it, message);

  return SUCCESS;
}
/****************************************************************************************/

Message *Client::read_message(int sockfd)
{
  int result = 0;

  uint8_t _type;
  result = read(sockfd, &_type, sizeof(uint8_t));
  if (result <= 0) {
    throw SocketClosedException();
  }
  Message::MESSAGE_TYPE type = static_cast<Message::MESSAGE_TYPE>(_type);

  switch (type) {

  case Message::SERVER: {
    uint8_t buf[ServerMessage::MESSAGE_SIZE - 1];
    memset(buf, 0, ServerMessage::MESSAGE_SIZE - 1);

    result = read(sockfd, buf, ServerMessage::MESSAGE_SIZE - 1);
    if (result <= 0) {
      throw SocketClosedException();
    }

    return ServerMessage::load_from_bytes(buf);
  }

  case Message::CLIENT: {
    uint8_t bytes[ClientMessage::MESSAGE_SIZE - 1];
    memset(bytes, 0, ClientMessage::MESSAGE_SIZE - 1);

    result = read(sockfd, bytes, ClientMessage::MESSAGE_SIZE - 1);
    if (result <= 0) {
      throw SocketClosedException();
    }

    if (recv_ctx.update_context(bytes)) {
      Message *ret = recv_ctx.msg;
      return ret;
    } else {
      return NULL;
    }
  }

  case Message::CONFIRM:
    break;

  default:
    return NULL;
  }

  return NULL;
}
/****************************************************************************************/

void Client::run_client(char *ip, int port)
{
  struct sockaddr_in servaddr;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  bzero(&servaddr, sizeof(servaddr));

  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(ip);
  servaddr.sin_port = htons(port);

  connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr));

  pthread_create(&reciever_thread, NULL, client_reciever_thread, this);
  pthread_create(&sender_thread, NULL, client_sender_thread, this);

  while (!socket_closed) {
    if (state_ctx.check_initialized()) {
      string s;
      getline(cin, s);
      if (!s.size()) {
        continue;
      }

      ClientMessage *send_msg = new ClientMessage(s, id);
      send_msg->print_message(nickname_map);
      send_msg->populate_timestamp();
      send_queue.push(send_msg);

      pthread_mutex_lock(&msg_lock);
      insert_message_in_chat(send_msg);
      pthread_mutex_unlock(&msg_lock);

    } else {
      Message *send_msg = update_context_send();
      if (send_msg != NULL) {
        send_queue.push(send_msg);
      }
    }
  }

  /** Clean up the threads and anything malloc'd */
  cout << "Connection to the server was lost.\n" << endl;
  pthread_kill(reciever_thread, SIGUSR1);
  pthread_kill(sender_thread, SIGUSR1);

  for (auto &i : msg_list) {
    delete i;
  }
}
/****************************************************************************************/

void *Client::client_reciever_thread(void *args)
{
  Client *client = (Client *)args;

  while (true) {
    try {
      Message *recv_msg = client->read_message(client->sockfd);
      if (recv_msg != NULL) {
        switch (recv_msg->get_type()) {
        case Message::SERVER:
          client->update_context_recv(recv_msg);
          break;
        case Message::CLIENT:
          pthread_mutex_lock(&client->msg_lock);
          reinterpret_cast<ClientMessage *>(recv_msg)->print_message(
              client->nickname_map);
          client->insert_message_in_chat(recv_msg);
          pthread_mutex_unlock(&client->msg_lock);
          break;
        default:
          break;
        }
      }
    } catch (SocketClosedException &e) {
      client->socket_closed = true;
    }
  }

  return NULL;
}
/****************************************************************************************/

void *Client::client_sender_thread(void *args)
{
  Client *client = (Client *)args;

  while (true) {
    Message *send_msg;
    client->send_queue.pop(&send_msg);
    for (auto &msg : send_msg->pack_to_bytes()) {
      send(client->sockfd, &msg[0], send_msg->get_message_size(), 0);
    }
    if (send_msg->get_type() == Message::CLIENT) {
      ClientMessage *c_msg = reinterpret_cast<ClientMessage *>(send_msg);
      for (auto &msg : c_msg->pack_to_bytes()) {
        vector<uint8_t> sum = c_msg->produce_checksum(msg);

        pthread_mutex_lock(&client->send_lock);
        client->send_list.push_back(sum);
        pthread_mutex_unlock(&client->send_lock);
      }
      pthread_mutex_lock(&client->msg_lock);
      client->insert_message_in_chat(send_msg);
      pthread_mutex_unlock(&client->msg_lock);
    }
  }

  return NULL;
}
/****************************************************************************************/

bool Client::ClientRecieverContext::update_context(uint8_t *buf)
{
  string _message((char *)&buf[BufferMessage::MESSAGE_OFFSET - 1]);
  int _idx =
      *(reinterpret_cast<uint32_t *>(&buf[BufferMessage::CURRENT_CHUNK_OFFSET - 1]));

  if (ctx_valid == false) {
    uint32_t _sender = *(reinterpret_cast<uint32_t *>(&buf[Message::SENDER_OFFSET - 1]));
    struct timeval _send_time =
        *(reinterpret_cast<struct timeval *>(&buf[Message::TIME_OFFSET - 1]));
    int _num_total =
        *(reinterpret_cast<uint32_t *>(&buf[BufferMessage::TOTAL_CHUNK_OFFSET - 1]));

    total_chunks = _num_total;
    msg = new ClientMessage(_message, _sender, _send_time);
    ctx_valid = true;
  } else {
    msg->append_to_message(_message);
  }

  if (_idx + 1 == total_chunks) {
    reset_context();
    return true;
  } else {
    return false;
  }
}
/****************************************************************************************/

void Client::ClientRecieverContext::reset_context() { ctx_valid = false; }
/****************************************************************************************/

void Client::update_context_recv(Message *recv_msg)
{
  state_ctx.update_context_recv(recv_msg);
}
/****************************************************************************************/

Message *Client::update_context_send() { return state_ctx.update_context_send(); }
/****************************************************************************************/

ClientStateContext::ClientStateContext(Client *_client)
{
  client = _client;
  initialized = false;
  state = START;
  pthread_mutex_init(&lock, NULL);
  sem_init(&sender_sem, 0, 0);

  recv_functors.set_default(&ClientStateContext::DEFAULT_RECV_Functor);
  register_functors();
}
/****************************************************************************************/

void ClientStateContext::register_functors()
{
  /* Register state behavior for the sender's client state handler. */
  REGISTER_CLIENT_SENDER_FUNCTOR(send_functors, START, ALL);
  REGISTER_CLIENT_SENDER_FUNCTOR(send_functors, REGISTERING_NAME, ALL);

  /* Register state behavior for the receiver's client state handler. */
  REGISTER_CLIENT_RECEIVER_FUNCTOR(recv_functors, REGISTERING_NAME, RESPONSE);
  REGISTER_CLIENT_RECEIVER_FUNCTOR(recv_functors, RECEIVING_CONTEXT, REGISTER_NAME);
  REGISTER_CLIENT_RECEIVER_FUNCTOR(recv_functors, RECEIVING_CONTEXT, RESPONSE);
  REGISTER_CLIENT_RECEIVER_FUNCTOR(recv_functors, READY, REGISTER_NAME);
  REGISTER_CLIENT_RECEIVER_FUNCTOR(recv_functors, READY, CONNECTION);
}
/****************************************************************************************/

void ClientStateContext::update_context_recv(Message *recv_msg)
{
  ServerMessage *_msg = reinterpret_cast<ServerMessage *>(recv_msg);
  pthread_mutex_lock(&lock);
  (this->*recv_functors.fetch_func(state, _msg->s_type))(_msg);
  pthread_mutex_unlock(&lock);
}
/****************************************************************************************/

Message *ClientStateContext::update_context_send()
{
  pthread_mutex_lock(&lock);
  Message *ret = (this->*send_functors.fetch_func(state))();

  pthread_mutex_unlock(&lock);
  return ret;
}
/****************************************************************************************/

bool ClientStateContext::check_initialized()
{
  bool ret = false;
  pthread_mutex_lock(&lock);
  ret = initialized;
  pthread_mutex_unlock(&lock);
  return ret;
}
/****************************************************************************************/

void ClientStateContext::wait_for_reciever()
{
  pthread_mutex_unlock(&lock);
  sem_wait(&sender_sem);
  pthread_mutex_lock(&lock);
}
/****************************************************************************************/

void ClientStateContext::wakeup_sender()
{
  int sem_val = 1;
  do {
    /* Spin out for a few cycles until the sender has caught up. */
    sem_getvalue(&sender_sem, &sem_val);
  } while (sem_val > 0);
  sem_post(&sender_sem);
}
/****************************************************************************************/

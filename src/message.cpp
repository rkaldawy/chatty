#include "message.hpp"
#include "server_message.hpp"
#include "utilities.hpp"

using namespace utilities;

void Message::populate_timestamp() { gettimeofday(&send_time, NULL); }
/****************************************************************************************/

bool Message::operator<(const Message &other)
{
  return timercmp(&send_time, &other.send_time, <);
}
/****************************************************************************************/

bool Message::operator>(const Message &other)
{
  return timercmp(&send_time, &other.send_time, >);
}
/****************************************************************************************/

vector<uint8_t> Message::pack_to_bytes_hlpr()
{
  vector<uint8_t> out;

  out.push_back(static_cast<uint8_t>(type));
  append_integer_to_bytes(sender, out);
  append_time_to_bytes(send_time, out);

  return out;
}
/****************************************************************************************/

ClientMessage::ClientMessage(string msg, uint32_t id) : Message(id, CLIENT)
{
  message = msg;
}
/****************************************************************************************/

ClientMessage::ClientMessage(string msg, uint32_t id, struct timeval tv)
    : Message(id, CLIENT, tv)
{
  message = msg;
}
/****************************************************************************************/

vector<vector<uint8_t>> ClientMessage::pack_to_bytes()
{
  vector<vector<uint8_t>> out;

  int num_chunks = (message.size() / (MESSAGE_CHUNK_SIZE - 1)) + 1;
  for (int i = 0; i < num_chunks; i++) {
    vector<uint8_t> chunk = Message::pack_to_bytes_hlpr();

    append_integer_to_bytes(num_chunks, chunk);
    append_integer_to_bytes(i, chunk);

    char buf[MESSAGE_CHUNK_SIZE] = {0};
    const char *start = message.c_str() + i * (MESSAGE_CHUNK_SIZE - 1);
    strncpy(buf, start, MESSAGE_CHUNK_SIZE - 1);
    std::vector<uint8_t> message_bytes(buf, buf + MESSAGE_CHUNK_SIZE);
    chunk.insert(chunk.end(), message_bytes.begin(), message_bytes.end());

    out.push_back(chunk);
  }

  return out;
}
/****************************************************************************************/

vector<uint8_t> ClientMessage::produce_checksum(vector<uint8_t> msg)
{
  return generate_md5(msg);
}
/****************************************************************************************/

void ClientMessage::print_message(std::map<uint32_t, string> &map)
{
  printf("<%s>: %s\n", map[sender].c_str(), message.c_str());
}
/****************************************************************************************/

void ClientMessage::append_to_message(string str) { message.append(str); }
/****************************************************************************************/

BufferMessage::BufferMessage(MESSAGE_TYPE _type, uint8_t *_buf, int _size)
    : Message(0, BUFFER)
{
  type = _type;
  size = _size;

  vector<uint8_t> tmp(_buf, _buf + size);
  buf = tmp;
}
/****************************************************************************************/

vector<vector<uint8_t>> BufferMessage::pack_to_bytes()
{
  vector<uint8_t> out;

  out.push_back(static_cast<uint8_t>(type));
  out.insert(out.end(), buf.begin(), buf.end());

  return {out};
}
/****************************************************************************************/

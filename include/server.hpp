#ifndef SERVER_H
#define SERVER_H

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>

#include "message.hpp"
#include "server_message.hpp"
#include "state_functors.hpp"
#include "utilities.hpp"

using namespace utilities;

class ServerConnection
{
public:
  ServerConnection(int _conn_fd);
  virtual ~ServerConnection() {}

  void start_connection();
  bool test_connection();

  static void *in_handler_thread(void *args);
  static void *out_handler_thread(void *args);

  bool socket_closed;
  Message *read_message(int sockfd);
  SemaphoreQueue<Message *> in_queue, out_queue;

  state_t state;
  bool initialized;
  int client_id; // We keep track of the client's ID.

private:
  pthread_t in_thread, out_thread;
  int conn_fd;
};

class Server
{
public:
  static const int MAX_CONNECTIONS = 20;
  static const int MAX_MESSAGES_HANDLED = 5;

  Server();
  virtual ~Server() {}

  void run_server(int port);
  static void *connection_handler_thread(void *args);
  void handle_message(ServerConnection *origin, Message *recv_msg);
  void handle_server_message(ServerConnection *origin, Message *recv_msg);

  int get_conns_size();

  void DEFAULT_Functor(ServerConnection *conn, Message *recv_msg) {}

  DECLARE_SERVER_FUNCTOR(START, REGISTER_NAME);

  void register_functors();

private:
  int server_fd;
  /* Chat state members */
  map<uint32_t, string> nickname_map;
  /* State machine functors */
  FunctorStateMap<decltype(&Server::DEFAULT_Functor)> functors;
  /* Connection handler members */
  pthread_t handler_thread;
  pthread_mutex_t conn_lock;
  sem_t conn_sem;
  vector<ServerConnection *> conns;
};

#endif

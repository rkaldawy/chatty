#ifndef STATE_FUNCTORS_H
#define STATE_FUNCTORS_H

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>

#include "message.hpp"
#include "server_message.hpp"

using namespace std;

template <typename T>
class FunctorWrapper
{
public:
  FunctorWrapper() {}
  FunctorWrapper(T _func) : func(_func) {}
  virtual ~FunctorWrapper() {}

  virtual bool _fetch_func(T &ret);

private:
  T func;
};

template <typename T>
class FunctorTypeMap
{
public:
  FunctorTypeMap() {}
  virtual ~FunctorTypeMap() {}

  virtual FunctorWrapper<T> &operator[](servermsg_t key);
  virtual bool _fetch_func(servermsg_t type, T &ret);

private:
  map<servermsg_t, FunctorWrapper<T>> f_map;
};

template <typename T>
class FunctorStateMap
{
public:
  FunctorStateMap() {}
  virtual ~FunctorStateMap() {}

  void set_default(T _def) { def = _def; }

  FunctorTypeMap<T> &operator[](state_t key);
  virtual bool _fetch_func(state_t state, servermsg_t type, T &ret);
  T fetch_func(state_t state);
  T fetch_func(state_t state, servermsg_t type);

private:
  T def;
  map<state_t, FunctorTypeMap<T>> f_map;
};

/****************************************************************************************/

#define DECLARE_CLIENT_SENDER_FUNCTOR(STATE, TYPE)                                       \
  _DECLARE_CLIENT_SENDER_FUNCTOR(STATE##_##TYPE##_Functor)

#define _DECLARE_CLIENT_SENDER_FUNCTOR(NAME) Message *NAME()

#define DEFINE_CLIENT_SENDER_FUNCTOR(STATE, TYPE)                                        \
  _DEFINE_CLIENT_SENDER_FUNCTOR(STATE##_##TYPE##_Functor)

#define _DEFINE_CLIENT_SENDER_FUNCTOR(NAME) Message *ClientStateContext::NAME()

/****************************************************************************************/

#define DECLARE_CLIENT_RECEIVER_FUNCTOR(STATE, TYPE)                                     \
  _DECLARE_CLIENT_RECEIVER_FUNCTOR(STATE##_##TYPE##_Functor)

#define _DECLARE_CLIENT_RECEIVER_FUNCTOR(NAME) void NAME(Message *recv_msg)

#define DEFINE_CLIENT_RECEIVER_FUNCTOR(STATE, TYPE)                                      \
  _DEFINE_CLIENT_RECEIVER_FUNCTOR(STATE##_##TYPE##_Functor)

#define _DEFINE_CLIENT_RECEIVER_FUNCTOR(NAME)                                            \
  void ClientStateContext::NAME(Message *recv_msg)

/****************************************************************************************/

#define DECLARE_SERVER_FUNCTOR(STATE, TYPE)                                              \
  _DECLARE_SERVER_FUNCTOR(STATE##_##TYPE##_Functor)

#define _DECLARE_SERVER_FUNCTOR(NAME)                                                    \
  void NAME(ServerConnection *origin, Message *recv_msg)

#define DEFINE_SERVER_FUNCTOR(STATE, TYPE)                                               \
  _DEFINE_SERVER_FUNCTOR(STATE##_##TYPE##_Functor)

#define _DEFINE_SERVER_FUNCTOR(NAME)                                                     \
  void Server::NAME(ServerConnection *origin, Message *recv_msg)

/****************************************************************************************/

#define REGISTER_CLIENT_RECEIVER_FUNCTOR(MAP, STATE, TYPE)                               \
  _REGISTER_FUNCTOR(MAP, STATE, TYPE, ClientStateContext::STATE##_##TYPE##_Functor)

#define REGISTER_CLIENT_SENDER_FUNCTOR(MAP, STATE, TYPE)                                 \
  _REGISTER_FUNCTOR(MAP, STATE, TYPE, ClientStateContext::STATE##_##TYPE##_Functor)

#define REGISTER_SERVER_FUNCTOR(MAP, STATE, TYPE)                                        \
  _REGISTER_FUNCTOR(MAP, STATE, TYPE, Server::STATE##_##TYPE##_Functor)

#define _REGISTER_FUNCTOR(MAP, STATE, TYPE, NAME)                                        \
  do {                                                                                   \
    try {                                                                                \
      MAP[STATE][TYPE] = FunctorWrapper<decltype(&NAME)>(&NAME);                         \
    } catch (...) {                                                                      \
      printf("WARNING: functor %s could not be registered. Skipping it...\n",            \
             QUOTE(NAME));                                                               \
    }                                                                                    \
  } while (0);

#define QUOTE(x) #x

/****************************************************************************************/

#endif

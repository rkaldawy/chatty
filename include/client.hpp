#ifndef CLIENT_H
#define CLIENT_H

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>

#include "message.hpp"
#include "server_message.hpp"
#include "state_functors.hpp"
#include "utilities.hpp"

using namespace utilities;

class Client;

class ClientStateContext
{
public:
  ClientStateContext(Client *_client);
  bool check_initialized();

  void wait_for_reciever();
  void wakeup_sender();

  // These functions should never be called directly
  void update_context_recv(Message *recv_msg);
  Message *update_context_send();

  // Defaults used to define mappings
  void DEFAULT_RECV_Functor(Message *recv_msg) {}
  Message *DEFAULT_SEND_Functor() { return NULL; }

  DECLARE_CLIENT_SENDER_FUNCTOR(START, ALL);
  DECLARE_CLIENT_SENDER_FUNCTOR(REGISTERING_NAME, ALL);

  DECLARE_CLIENT_RECEIVER_FUNCTOR(REGISTERING_NAME, RESPONSE);
  DECLARE_CLIENT_RECEIVER_FUNCTOR(RECEIVING_CONTEXT, REGISTER_NAME);
  DECLARE_CLIENT_RECEIVER_FUNCTOR(RECEIVING_CONTEXT, RESPONSE);
  DECLARE_CLIENT_RECEIVER_FUNCTOR(READY, REGISTER_NAME);
  DECLARE_CLIENT_RECEIVER_FUNCTOR(READY, CONNECTION);

  void register_functors();

private:
  Client *client;

  pthread_mutex_t lock;
  state_t state;
  bool initialized;
  sem_t sender_sem;

  FunctorStateMap<decltype(&ClientStateContext::DEFAULT_SEND_Functor)> send_functors;
  FunctorStateMap<decltype(&ClientStateContext::DEFAULT_RECV_Functor)> recv_functors;
};

class Client
{
public:
  class ClientRecieverContext
  {
  public:
    ClientRecieverContext() : ctx_valid(false) {}

    void reset_context();
    bool update_context(uint8_t *buf);

    ClientMessage *msg;

  private:
    bool ctx_valid;
    int total_chunks;
    int chunks_collected;
  };

  friend class ClientStateContext;

  void update_context_recv(Message *recv_msg);
  Message *update_context_send();

  static const int SUCCESS = 0;
  static const int INVALID_MESSAGE = -1;

  Client();

  virtual ~Client() {}

  void run_client(char *ip, int port);

  int insert_message_in_chat(Message *message);

  static void *client_reciever_thread(void *args);
  static void *client_sender_thread(void *args);

  Message *read_message(int sockfd);

  bool socket_closed;
  uint32_t id;

private:
  uint32_t counter;
  int sockfd;
  /* Client state members */
  string nickname;
  map<uint32_t, string> nickname_map;
  /* Client recieve/send thread members */
  ClientRecieverContext recv_ctx;

  ClientStateContext state_ctx;

  pthread_t reciever_thread, sender_thread;

  SemaphoreQueue<Message *> send_queue;

  pthread_mutex_t msg_lock;
  vector<Message *> msg_list;

  pthread_mutex_t send_lock;
  vector<vector<uint8_t>> send_list;
};

#endif

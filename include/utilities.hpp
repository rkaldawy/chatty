#ifndef UTILITIES_H
#define UTILITIES_H

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>

using namespace std;

namespace utilities
{

template <typename T>
class SemaphoreQueue : public queue<T>
{
public:
  static const int MAX_SIZE = 100;

  SemaphoreQueue();
  virtual ~SemaphoreQueue();

  int push(const T &val, bool trywait = false);
  int pop(T *val, bool trywait = false);

  template <class... Args>
  void emplace(Args &&... args)
  {
    return;
  }

private:
  pthread_mutex_t queue_lock;
  sem_t empty_sem;
  sem_t full_sem;
};

struct SocketClosedException : public exception
{
  const char *what() const throw() { return "Socket is closed"; }
};

vector<uint8_t> generate_md5(vector<uint8_t> buf);

void append_time_to_bytes(const struct timeval &tv, vector<uint8_t> &result);
void append_integer_to_bytes(uint32_t val, vector<uint8_t> &result);
void append_integer_to_bytes(uint32_t val, uint8_t *result);

}; // namespace utilities

#endif

#ifndef SERVER_MESSAGE_H
#define SERVER_MESSAGE_H

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>

#include "message.hpp"

using namespace std;

enum state_t { START, REGISTERING_NAME, RECEIVING_CONTEXT, READY };
enum servermsg_t { DEFAULT, ALL, REGISTER_NAME, RESPONSE, CONNECTION };

class ServerMessage : public Message
{
public:
  static const int STYPE_OFFSET = TIME_OFFSET + sizeof(struct timeval);

  ServerMessage() {}
  ServerMessage(servermsg_t _type, uint32_t id);
  ServerMessage(servermsg_t _type, uint32_t id, struct timeval tv);

  virtual ~ServerMessage(){};

  static const int DATA_SIZE = 256;
  static const int MESSAGE_SIZE = Message::MESSAGE_SIZE + 1 + DATA_SIZE;

  virtual vector<vector<uint8_t>> pack_to_bytes();
  static ServerMessage *load_from_bytes(uint8_t *buf);

  virtual ServerMessage *clone() { return new ServerMessage(*this); };
  virtual int get_message_size() { return MESSAGE_SIZE; };

  servermsg_t s_type;

protected:
  virtual vector<uint8_t> pack_to_bytes_hlpr();
};

class RegisterName_SM : public ServerMessage
{
public:
  static const int ID_OFFSET = STYPE_OFFSET + 1;
  static const int NICKNAME_OFFSET = ID_OFFSET + sizeof(uint32_t);

  RegisterName_SM(uint32_t _id, string _nname);
  RegisterName_SM(uint32_t _id, struct timeval tv, string _nname);

  virtual ~RegisterName_SM(){};

  static const int MESSAGE_SIZE = ServerMessage::MESSAGE_SIZE;

  virtual vector<vector<uint8_t>> pack_to_bytes();

  virtual ServerMessage *clone() { return new RegisterName_SM(*this); };
  virtual int get_message_size() { return MESSAGE_SIZE; };

  uint32_t id;
  string nname;
};

class Response_SM : public ServerMessage
{
public:
  static const int RESP_OFFSET = STYPE_OFFSET + 1;

  Response_SM(uint32_t _id, bool _resp);
  Response_SM(uint32_t _id, struct timeval tv, bool _resp);

  virtual ~Response_SM(){};

  static const int MESSAGE_SIZE = ServerMessage::MESSAGE_SIZE;

  virtual vector<vector<uint8_t>> pack_to_bytes();

  virtual ServerMessage *clone() { return new Response_SM(*this); };
  virtual int get_message_size() { return MESSAGE_SIZE; };

  bool resp;
};

class Connection_SM : public ServerMessage
{
public:
  static const bool CONNECTED = true;
  static const bool DISCONNECTED = false;

  static const int ID_OFFSET = STYPE_OFFSET + 1;
  static const int STATUS_OFFSET = ID_OFFSET + sizeof(uint32_t);

  Connection_SM(uint32_t _id, uint32_t _client, bool _status);
  Connection_SM(uint32_t _id, struct timeval tv, uint32_t _client, bool _status);

  virtual ~Connection_SM(){};

  static const int MESSAGE_SIZE = ServerMessage::MESSAGE_SIZE;

  virtual vector<vector<uint8_t>> pack_to_bytes();

  virtual ServerMessage *clone() { return new Connection_SM(*this); };
  virtual int get_message_size() { return MESSAGE_SIZE; };

  uint32_t client;
  bool status;
};

#endif

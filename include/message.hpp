#ifndef MESSAGE_H
#define MESSAGE_H

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/evp.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <vector>

using namespace std;

class Message {
public:
  /* Message types */
  enum MESSAGE_TYPE { NONE, SERVER, CLIENT, CONFIRM, BUFFER };

  static const int HASH_SIZE = 16;

  static const int TYPE_OFFSET = 0;
  static const int SENDER_OFFSET = TYPE_OFFSET + 1;
  static const int TIME_OFFSET = SENDER_OFFSET + sizeof(uint32_t);

  Message() {}
  Message(uint32_t id, MESSAGE_TYPE t) : type(t), sender(id) {}
  Message(uint32_t id, MESSAGE_TYPE t, struct timeval tv)
      : type(t), sender(id), send_time(tv) {}

  virtual ~Message() {}

  static const int MESSAGE_SIZE = 1 + 4 + sizeof(struct timeval);
  virtual int get_message_size() { return MESSAGE_SIZE; };

  virtual vector<vector<uint8_t>> pack_to_bytes() { return {}; };

  MESSAGE_TYPE get_type() { return type; }
  virtual Message *clone() { return new Message(*this); }

  void populate_timestamp();

  /* Compares only the timevals of the two messages.*/
  bool operator<(const Message &other);
  bool operator>(const Message &other);

protected:
  vector<uint8_t> pack_to_bytes_hlpr();
  MESSAGE_TYPE type;
  uint32_t sender;
  struct timeval send_time;
};

class BufferMessage : public Message {
public:
  static const int TOTAL_CHUNK_OFFSET = TIME_OFFSET + sizeof(struct timeval);
  static const int CURRENT_CHUNK_OFFSET = TOTAL_CHUNK_OFFSET + sizeof(uint32_t);
  static const int MESSAGE_OFFSET = CURRENT_CHUNK_OFFSET + sizeof(uint32_t);

  BufferMessage(MESSAGE_TYPE _type, uint8_t *_buf, int _size);

  virtual vector<vector<uint8_t>> pack_to_bytes();

  virtual BufferMessage *clone() { return new BufferMessage(*this); }
  virtual int get_message_size() { return size; };

private:
  MESSAGE_TYPE type;
  vector<uint8_t> buf;
  int size;
};

class ClientMessage : public Message {
public:
  static const int TOTAL_CHUNK_OFFSET = TIME_OFFSET + sizeof(struct timeval);
  static const int CURRENT_CHUNK_OFFSET = TOTAL_CHUNK_OFFSET + sizeof(uint32_t);
  static const int MESSAGE_OFFSET = CURRENT_CHUNK_OFFSET + sizeof(uint32_t);

  ClientMessage() {}
  ClientMessage(string msg, uint32_t id);
  ClientMessage(string msg, uint32_t id, struct timeval tv);

  virtual ~ClientMessage() {}

  static const int MESSAGE_CHUNK_SIZE = 256;
  static const int MESSAGE_SIZE =
      Message::MESSAGE_SIZE + 4 + 4 + MESSAGE_CHUNK_SIZE;

  virtual vector<vector<uint8_t>> pack_to_bytes();

  virtual ClientMessage *clone() { return new ClientMessage(*this); };
  virtual int get_message_size() { return MESSAGE_SIZE; };

  void append_to_message(string str);
  vector<uint8_t> produce_checksum(vector<uint8_t> msg);

  /* Temporary, until the chat frontend is implemented */
  void print_message(std::map<uint32_t, string> &map);

private:
  string message;
};

class ConfirmMessage : public Message {
public:
  ConfirmMessage() {}
  ConfirmMessage(char *buf, uint32_t id) : Message(id, CONFIRM) {
    memcpy(hash, buf, HASH_SIZE);
  }

  static const int MESSAGE_SIZE = Message::MESSAGE_SIZE + 16;

  virtual vector<vector<uint8_t>> pack_to_bytes();

  virtual ConfirmMessage *clone() { return new ConfirmMessage(*this); };
  virtual int get_message_size() { return MESSAGE_SIZE; };

private:
  char hash[HASH_SIZE];
};

#endif
